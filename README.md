# Legacy Project Maintenance Pipeline

  + Let's suppose that you have to maintain an obsolete project with
    legacy code.

  + Checking all the codebase via CI would result in hundreds of errors
    and warnings.

  + This [.gitlab-ci.yml](.gitlab-ci.yml) executes CI only for feature
    branches, i.e. branches other than the default one.

  + It considers only the newly added and modified files and checks them
    against Groovy, Python, and Shell linters.

  + If a new file is _added_ to the feature branch and it doesn't pass a
    linter check, this is indicated as a failure.

  + If a check fails for a _modified_ file, just a warning is printed.

  + The developer who opens a relevant merge request must check if the
    latter failure occurs in the lines they modified.
